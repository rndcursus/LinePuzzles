package linepuzzlept12.linepuzzle;

import android.content.Context;

/**
 * Created by mattijn on 26/04/17.
 */

public class MapsGenerator {
    private  Context context;
    public MapsGenerator(Context context){
        this.context = context;
    }
    public Grid getMapA1(){
        int n = context.getResources().getInteger(R.integer.a1width);  //width in boxes
        int m = context.getResources().getInteger(R.integer.a1height);  //height in boxes
        int[] x_coords = context.getResources().getIntArray(R.array.a1x); //the x coords of the boxes width triangles
        int[] y_coords = context.getResources().getIntArray(R.array.a1y); //the y coords of the boxes with triangles
        int[] triangles = context.getResources().getIntArray(R.array.a1triangles); //the number of triangles ont the box at x_coords[i], y_coords[i]
        int start_x = context.getResources().getInteger(R.integer.a1startx);
        int start_y = context.getResources().getInteger(R.integer.a1starty);
        int end_x = context.getResources().getInteger(R.integer.a1endx);
        int end_y = context.getResources().getInteger(R.integer.a1endy);
        MapACreator mapACreator  = new MapACreator(m,n,x_coords,y_coords,triangles,start_x,start_y,end_x,end_y);
        Grid grid = mapACreator.generateGrid();
        return grid;
    }
}
