package linepuzzlept12.linepuzzle;

/**
 * Created by mattijn on 20/04/17.
 */

public class LineDot extends Line {

    private boolean hasDot = false;

    public boolean hasDot() {
        return hasDot;
    }

    public void setDot() {
        hasDot =  true;
    }
}
