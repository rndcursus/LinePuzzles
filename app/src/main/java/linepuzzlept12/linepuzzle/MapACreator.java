package linepuzzlept12.linepuzzle;

import android.graphics.Point;

/**
 * Created by mattijn on 26/04/17.
 */

public class MapACreator {
    private final int[] triangles;
    private final int heigth;
    private final int width;
    private final int[] x;
    private final int[] y;
    private final Point start;
    private final Point end;

    public MapACreator(int  heigth, int width,
                       int[] x, int[] y, int[] triangles,
                       int startX, int startY, int endX, int endY){
        this.heigth = heigth;
        this.width = width;
        this.x = x;
        this.y = y;
        this.triangles =triangles;
        this.start = new Point(startX, startY);
        this.end  = new Point(endX, endY);
    }

    public Grid generateGrid(){
        return new GridA(width, heigth, start, end, new MapSettingsGenerator() {
            @Override
            public BoxTriangle setTriangles(BoxTriangle boxTriangle) {
                for (int i = 0; i < x.length; i++){
                    if (x[i] == boxTriangle.getX() && y[i] == boxTriangle.getY()){
                        boxTriangle.setTriangles(triangles[i]);
                        return boxTriangle;
                    }
                }
                return boxTriangle;
            }

            @Override
            public Line setDot(Line line, int boxX, int boxY, LocationInBox loc) {
                return line;
            }

            @Override
            public Intersection setDot(Intersection intersection, int boxX, int boxY, LocationInBox loc) {
                return intersection;
            }
        });
    }
}
