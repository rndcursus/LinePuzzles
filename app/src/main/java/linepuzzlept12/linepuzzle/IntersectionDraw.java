package linepuzzlept12.linepuzzle;

import android.graphics.Paint;

/**
 * Created by steff on 26-4-2017.
 */

public class IntersectionDraw {
    private float x0;
    private float y0;
    private float x1;
    private float y1;
    private Paint paint;
    private Intersection intersection;

    public IntersectionDraw(float x0, float y0, float x1, float y1, Paint paint, Intersection intersection) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
        this.paint = paint;
        this.intersection = intersection;
    }

    public float getx0() {
        return x0;
    }

    public float gety0() {
        return y0;
    }

    public float getx1() {
        return x1;
    }

    public float gety1() {
        return y1;
    }

    public Paint getPaint() {
        return paint;
    }

    public Intersection getIntersect(){return intersection; }
}
