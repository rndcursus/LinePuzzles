package linepuzzlept12.linepuzzle;

/**
 * Created by Mark ten Klooster on 19-4-2017.
 */

public class BoxTriangle extends Box{

    private int numberOfTriangles;

    public BoxTriangle(int x, int y, Line topLine, Line bottomLine, Line leftLine, Line rightLine, Intersection topLeft, Intersection topRight, Intersection bottomLeft, Intersection bottomRight, int numberOfTriangles){
        super(x,y,topLine, bottomLine, leftLine, rightLine, topLeft, topRight, bottomLeft, bottomRight);
        this.numberOfTriangles = numberOfTriangles;
    }

    public int getNumberOfTriangles() {
        return numberOfTriangles;
    }

    public void setTriangles(int triangles) {
        this.numberOfTriangles = triangles;
    }
}