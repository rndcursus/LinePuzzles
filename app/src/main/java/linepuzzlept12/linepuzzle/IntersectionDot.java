package linepuzzlept12.linepuzzle;

/**
 * Created by Mark ten Klooster on 26-4-2017.
 */

public class IntersectionDot extends Intersection {

    private boolean hasDot = false;

    public IntersectionDot(boolean hasDot){
        super();
        this.hasDot = hasDot;
    }

    public boolean hasDot() {
        return hasDot;
    }

    public void setDot() {
        this.hasDot = true;
    }
}
