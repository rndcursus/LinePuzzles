package linepuzzlept12.linepuzzle;

import java.util.Iterator;

/**
 * Created by Mark ten Klooster on 19-4-2017.
 */

public class Box {


    private final int x;
    private final int y;

    protected Line topLine;
    protected Line bottomLine;
    protected Line leftLine;
    protected Line rightLine;

    protected Intersection topLeft;
    protected Intersection topRight;
    protected Intersection bottomLeft;
    protected Intersection bottomRight;

    public Box(int x, int y,
               Line topLine, Line bottomLine, Line leftLine, Line rightLine,
               Intersection topLeft, Intersection topRight, Intersection bottomLeft, Intersection bottomRight){
        this.topLine = topLine;
        this.bottomLine = bottomLine;
        this.leftLine = leftLine;
        this.rightLine = rightLine;

        this.topLeft = topLeft;
        this.topRight = topRight;
        this.bottomLeft = bottomLeft;
        this.bottomRight = bottomRight;
        this.x = x;
        this.y = y;
    }

    public Line getTopLine() {
        return topLine;
    }

    public Line getBottomLine() {
        return bottomLine;
    }

    public Line getLeftLine() {
        return leftLine;
    }

    public Line getRightLine() {
        return rightLine;
    }

    public Intersection getTopLeft() {
        return topLeft;
    }

    public Intersection getTopRight() {
        return topRight;
    }

    public Intersection getBottomLeft() {
        return bottomLeft;
    }

    public Intersection getBottomRight() {
        return bottomRight;
    }
    public int activatedLines(){
        int result = 0;
        if (topLine.getStatus()){
            result++;
        }
        if (bottomLine.getStatus()){
            result++;
        }
        if (leftLine.getStatus()){
            result++;
        }
        if (rightLine.getStatus()){
            result++;
        }
        return result;
    }
    public Iterator<Line> getLines(){
        return new Iterator<Line>() {
            private int current = 0;
            @Override
            public boolean hasNext() {
                return current > 4;
            }

            @Override
            public Line next() {
                if (current  == 0){
                    current++;
                    return topLine;
                }
                if (current  == 1){
                    current++;
                    return leftLine;
                }
                if (current  == 2){
                    current++;
                    return rightLine;
                }
                if (current  == 3){
                    current++;
                    return bottomLine;
                }
                return null;
            }
        };
    }
    public Iterator<Intersection> getIntersections(){
        return new Iterator<Intersection>() {
            private int current = 0;
            @Override
            public boolean hasNext() {
                return current > 4;
            }

            @Override
            public Intersection next() {
                if (current  == 0){
                    current++;
                    return topLeft;
                }
                if (current  == 1){
                    current++;
                    return topRight;
                }
                if (current  == 2){
                    current++;
                    return bottomLeft;
                }
                if (current  == 3){
                    current++;
                    return bottomRight;
                }
                return null;
            }
        };
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
