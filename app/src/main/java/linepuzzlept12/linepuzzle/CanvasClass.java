package linepuzzlept12.linepuzzle;

import android.graphics.Canvas;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by steff on 20-4-2017.
 */

public class CanvasClass extends View {

    private Paint mPaint;
    Canvas canvas;
    private float x0,y0,x1,y1;
    LinkedList<LineDraw> lines = new LinkedList<>();
    LinkedList<IntersectionDraw> intersections = new LinkedList<>();
     LinkedList<Path> triangles = new LinkedList<>();
    private float LineWidth = 50; //// TODO: 26/04/17 Magic!!!!
   

    public CanvasClass (Context context){
        super(context);
        mPaint = new Paint();
        mPaint.setColor(Color.BLACK);
        this.canvas = new Canvas();

    }

    @Override
    public void onDraw (Canvas canvas){
        super.onDraw(canvas);
        this.canvas = canvas;
        canvas.drawColor(Color.rgb(0,0,2)); //// TODO: 26/04/17 Magic!!!
       // mPaint.setColor(Color.rgb(129,110,44));
        for(int i = 0; i < lines.size(); i++) {
           LineDraw line =  lines.get(i);
           canvas.drawLine(line.getx0(), line.gety0(), line.getx1(), line.gety1(), line.getPaint());
        }

        for(int i = 0; i < intersections.size(); i++) {
            IntersectionDraw intersect =  intersections.get(i);
            canvas.drawLine(intersect.getx0(), intersect.gety0(), intersect.getx1(), intersect.gety1(), intersect.getPaint());
        }

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(0);
        paint.setColor(android.graphics.Color.rgb(255, 195, 0));
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);
        for (int i = 0; i < triangles.size(); i++) {
            canvas.drawPath(triangles.get(i), paint);
        }
    }

    public void clearDraw(){
        draw(canvas);
        invalidate();
    }

    public void drawLineOn(int x0, int y0, int x1, int y1, Line line){
        if(line.getStatus()) mPaint.setColor(Color.rgb(255,250,160)); //// TODO: 26/04/17 Magic!!
        else mPaint.setColor(Color.rgb(129,110,44)); //// TODO: 26/04/17 Magic!!
        mPaint.setStrokeWidth(LineWidth);

        for(int i=0; i < lines.size(); i++){
            if(lines.get(i).getLine() == line) lines.remove(i);
        }
        lines.add(new LineDraw((float) x0, (float) y0, (float) x1, (float) y1, new Paint(mPaint), line));
    }

    public void drawIntersection(int x, int y, Intersection intersection){
        if(intersection.getStatus()) mPaint.setColor(Color.rgb(255,250,160)); //// TODO: 26/04/17 Magic!!
        else mPaint.setColor(Color.rgb(129,110,44));//// TODO: 26/04/17 Magic!!
        mPaint.setStrokeWidth(LineWidth);

        intersections.add(new IntersectionDraw((float) (x-(0.5*LineWidth)), (float) y , (float) (x + (0.5*LineWidth)),(float) y, mPaint, intersection)); //// TODO: 26/04/17 magic!!  de 0.5
    }

    public void drawTriangle(float x, float y){
        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        int size = 50;
        path.moveTo(x-size/2,y+size/2);
        path.lineTo(x+size/2,y+size/2);
        path.lineTo(x,y-size/2);
        path.lineTo(x-size/2,y+size/2);
        path.close();
        triangles.add(path);
    }
}

