package linepuzzlept12.linepuzzle;

/**
 * Created by Robin on 26-4-2017.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.widget.RelativeLayout;
import android.widget.Toast;

import static android.content.ContentValues.TAG;
import static linepuzzlept12.linepuzzle.R.layout.activity_game1;

public class GameActivity1 extends Activity {

    private GestureDetectorCompat mDetector;
    private CanvasClass canvas;
    private Grid grid;
    private int boxSize;
    int screenWidth;
    int screenHeight;
    private Point current;
    private int offsetX;
    private int offsetY;
    private CheckerA checker = new CheckerA();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_game1);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        offsetX = (int)(screenWidth*0.03); //// TODO: 26/04/17 Magic!!
        offsetY = (int)(screenHeight*0.03); //// TODO: 26/04/17 Magic!!
        MapsGenerator mapsGenerator = new MapsGenerator(getApplicationContext());
        Grid grid = mapsGenerator.getMapA1();
        this.grid = grid;
        grid.fillGrid(); // Fill the Grid
        int boxSize = Math.min((screenWidth-offsetX*2)/this.grid.getWidth(),(screenHeight-offsetY*2)/this.grid.getHeight());

        this.boxSize = boxSize;
        current = grid.getStart();
        createCanvas();

        mDetector = new GestureDetectorCompat(this, new MyGestureListener());

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    public void createCanvas(){
        CanvasClass canvasClass = new CanvasClass(this);
        RelativeLayout canvasLayout = (RelativeLayout) findViewById(R.id.canvasLayout);
        canvasLayout.addView(canvasClass);
        canvas  = canvasClass;
        printGrid();
    }

    public void printGrid(){
        for(int i=0; i < this.grid.getWidth(); i++){
            for(int j=0; j < this.grid.getHeight(); j++){
                Box box = grid.getBox(i,j);
                if(box instanceof BoxTriangle) {
                    BoxTriangle boxTriangle = (BoxTriangle) box;
                    if (boxTriangle.getNumberOfTriangles() == 1) {
                        canvas.drawTriangle((float) (i + 0.5) * boxSize + offsetX, (float) (j + 0.5) * boxSize + offsetY);
                    }
                    else if(boxTriangle.getNumberOfTriangles() == 2) {
                        canvas.drawTriangle((float)(i + 0.4) * boxSize + offsetX, (float) (j + 0.5) * boxSize + offsetY);
                        canvas.drawTriangle((float)(i + 0.6) * boxSize + offsetX, (float) (j + 0.5) * boxSize + offsetY);
                    }
                    else if (boxTriangle.getNumberOfTriangles() == 3) {
                        canvas.drawTriangle((float)(i + 0.3) * boxSize + offsetX, (float)(j + 0.5) * boxSize + offsetY);
                        canvas.drawTriangle((float)(i + 0.5) * boxSize + offsetX, (float)(j + 0.5) * boxSize + offsetY);
                        canvas.drawTriangle((float)(i + 0.7) * boxSize + offsetX, (float)(j + 0.5) * boxSize + offsetY);
                    }
                }

                //TopLine
                canvas.drawLineOn(i*boxSize+offsetX, j*boxSize+offsetY, (i+1)*boxSize+offsetX,j*boxSize+offsetY, box.getTopLine());

                //LeftLine
                canvas.drawLineOn(i*boxSize+offsetX, j*boxSize+offsetY, (i)*boxSize+offsetX,(j+1)*boxSize+offsetY, box.getLeftLine());

                //BottomLine
                canvas.drawLineOn(i*boxSize+offsetX, (j+1)*boxSize+offsetY, (i+1)*boxSize+offsetX,(j+1)*boxSize+offsetY, box.getBottomLine());

                //RightLine
                canvas.drawLineOn((i+1)*boxSize+offsetX, j*boxSize+offsetY, (i+1)*boxSize+offsetX,(j+1)*boxSize+offsetY, box.getRightLine());

                // bottomLeftIntersect
                canvas.drawIntersection((i+1)*boxSize+offsetX, j*boxSize+offsetY, box.getBottomLeft());
                // topLeftIntersect
                canvas.drawIntersection(i*boxSize+offsetX, j*boxSize+offsetY, box.getTopLeft());
                // bottomRightIntersect
                canvas.drawIntersection((i+1)*boxSize+offsetX, (j+1)*boxSize+offsetY, box.getBottomRight());
                // topRightIntersect
                canvas.drawIntersection(i*boxSize+offsetX, (j+1)*boxSize+offsetY, box.getTopRight());
            }
        }
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            Log.d(TAG, "onFling: " + e1.toString()+e2.toString());

            float x1 = e1.getX();
            float y1 = e1.getY();

            float x2 = e2.getX();
            float y2 = e2.getY();

            if((x1-x2) > 100 && Math.abs(x1-x2) > Math.abs(y1-y2) )
            {
                moveLeft();
                if(isFinish()) check(grid);
                return true;
            }
            else if((x1-x2) < -100 && Math.abs(x1-x2) > Math.abs(y1-y2) )
            {
                moveRight();
                if(isFinish()) check(grid);
                return true;
            }
            else if((y1-y2) > 100 && Math.abs(x1-x2) < Math.abs(y1-y2) )
            {
                moveUp();
                if(isFinish()) check(grid);
                return true;
            }
            else if((y1-y2) < -100 && Math.abs(x1-x2) < Math.abs(y1-y2) )
            {
                moveDown();
                if(isFinish()) check(grid);
                return true;
            }

            return false;
        }

    }
    private void moveRight(){
        int x= current.x;
        int y = current.y;
        if(x!=grid.getWidth()){
            if(y==0){
                if (grid.getBox(x, y).getTopLine().getStatus()) {
                    grid.getBox(x, y).getTopLine().setStatus(false);
                } else grid.getBox(x, y).getTopLine().setStatus(true);
            }
            else{
                if (grid.getBox(x, y - 1).getBottomLine().getStatus()) {
                    grid.getBox(x, y - 1).getBottomLine().setStatus(false);

                } else grid.getBox(x, y - 1).getBottomLine().setStatus(true);
            }
            current.set(x + 1, y);
            createCanvas();
        }

    }
    private void moveLeft(){
        int x= current.x;
        int y = current.y;
        if(x!=0){
            if(y!=0){
                if (grid.getBox(x-1, y-1).getBottomLine().getStatus()) {
                    grid.getBox(x-1, y-1).getBottomLine().setStatus(false);
                }
                else {
                    grid.getBox(x-1, y-1).getBottomLine().setStatus(true);
                }
            }
            else{
                if (grid.getBox(x-1, y).getTopLine().getStatus()) {
                    grid.getBox(x-1, y).getTopLine().setStatus(false);
                }
                else {
                    grid.getBox(x-1, y).getTopLine().setStatus(true);
                }
            }
            current.set(x - 1, y);
        }
        createCanvas();
    }
    private void moveUp(){
        int x= current.x;
        int y = current.y;
        if(y!=0){
            if(x==grid.getWidth()){
                if (grid.getBox(x-1, y-1).getRightLine().getStatus()) {
                    grid.getBox(x-1, y-1).getRightLine().setStatus(false);
                }
                else {
                    grid.getBox(x-1, y-1).getRightLine().setStatus(true);
                }
            }
            else{
                if (grid.getBox(x, y-1).getLeftLine().getStatus()) {
                    grid.getBox(x, y-1).getLeftLine().setStatus(false);
                }
                else {
                    grid.getBox(x, y-1).getLeftLine().setStatus(true);
                }
            }
            current.set(x,y-1);
        }
        createCanvas();
    }
    private void moveDown(){
        int x= current.x;
        int y = current.y;
        if(y!=grid.getHeight()){
            if(x==grid.getWidth()){
                if (grid.getBox(x-1, y).getRightLine().getStatus()) {
                    grid.getBox(x-1, y).getRightLine().setStatus(false);
                }
                else {
                    grid.getBox(x-1, y).getRightLine().setStatus(true);
                }
            }
            else{
                if (grid.getBox(x, y).getLeftLine().getStatus()) {
                    grid.getBox(x, y).getLeftLine().setStatus(false);
                }
                else {
                    grid.getBox(x, y).getLeftLine().setStatus(true);
                }
            }
            current.set(x,y+1);
        }
        createCanvas();
    }

    private boolean isFinish(){
        return (current.equals(grid.getEnd()));
    }

    private void check(Grid grid){
       if(grid.validate()){
           Context context = getApplicationContext();
           CharSequence text = "You won!";
           int duration = Toast.LENGTH_SHORT;

           Toast toast = Toast.makeText(context, text, duration);
           toast.show();
       }
       else{
           Context context = getApplicationContext();
           CharSequence text = "This wasn't right!";
           int duration = Toast.LENGTH_SHORT;

           Toast toast = Toast.makeText(context, text, duration);
           toast.show();
       }

    }
}