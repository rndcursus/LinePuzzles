package linepuzzlept12.linepuzzle;

import java.util.Iterator;

/**
 * Created by mattijn on 20/04/17.
 */

public class CheckerB implements OnFinishedListener{
    @Override
    public boolean check(Grid grid) {
        Iterator<Box> boxes = grid.getBoxes();
        while(boxes.hasNext()){
            Box box = boxes.next();
            Iterator<Line> lines = box.getLines();
            while(lines.hasNext()){
                Line l = lines.next();
                LineDot line = (LineDot)  l;
                if (line.hasDot() && !line.getStatus()){
                    return false;
                }
            }
            Iterator<Intersection> intersections = box.getIntersections();
            while(intersections.hasNext()){
                Intersection i = intersections.next();
                IntersectionDot intersection = (IntersectionDot)  i;
                if (intersection.hasDot() && !intersection.getStatus()){
                    return false;
                }
            }
        }
        return true;

    }
}
