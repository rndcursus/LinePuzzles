package linepuzzlept12.linepuzzle;

/**
 * Created by mattijn on 19/04/17.
 */

public interface OnFinishedListener {
    public boolean check(Grid grid);

}
