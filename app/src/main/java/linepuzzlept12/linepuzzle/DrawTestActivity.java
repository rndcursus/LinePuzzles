package linepuzzlept12.linepuzzle;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class DrawTestActivity extends AppCompatActivity {

    CanvasClass canvas;
    Grid grid;
    int boxSize;
    int screenWidth;
    int screenHeight;
    int offsetX;
    int offsetY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_test);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        offsetX = (int)(screenWidth*0.03); //// TODO: 26/04/17 Magic!!
        offsetY = (int)(screenHeight*0.03); //// TODO: 26/04/17 Magic!!
        MapsGenerator mapsGenerator = new MapsGenerator(getApplicationContext());
        this.grid = mapsGenerator.getMapA1();
        grid.fillGrid(); // Fill the Grid
        int boxSize = Math.min((screenWidth-offsetX*2)/grid.getWidth(),(screenHeight-offsetY*2)/grid.getHeight());
        this.boxSize = boxSize;
        createCanvas();
    }

    public void createCanvas(){
        CanvasClass canvasClass = new CanvasClass(this);
        RelativeLayout canvasLayout = (RelativeLayout) findViewById(R.id.canvasLayout);
        canvasLayout.addView(canvasClass);
        canvas  = canvasClass;
        printGrid();
    }

    public void resetCanvas(){
        canvas.clearDraw();
    }

    public void printGrid(){
        for(int i=0; i < this.grid.getWidth(); i++){
            for(int j=0; j < this.grid.getHeight(); j++){
                Box box = grid.getBox(i,j);
                if(box instanceof BoxTriangle) {
                    BoxTriangle boxTriangle = (BoxTriangle) box;
                    if (boxTriangle.getNumberOfTriangles() == 1) {
                        canvas.drawTriangle((float) (i + 0.5) * boxSize + offsetX, (float) (j + 0.5) * boxSize + offsetY);
                    }
                    else if(boxTriangle.getNumberOfTriangles() == 2) {
                        canvas.drawTriangle((float)(i + 0.4) * boxSize + offsetX, (float) (j + 0.5) * boxSize + offsetY);
                        canvas.drawTriangle((float)(i + 0.6) * boxSize + offsetX, (float) (j + 0.5) * boxSize + offsetY);
                    }
                    else if (boxTriangle.getNumberOfTriangles() == 3) {
                        canvas.drawTriangle((float)(i + 0.3) * boxSize + offsetX, (float)(j + 0.5) * boxSize + offsetY);
                        canvas.drawTriangle((float)(i + 0.5) * boxSize + offsetX, (float)(j + 0.5) * boxSize + offsetY);
                        canvas.drawTriangle((float)(i + 0.7) * boxSize + offsetX, (float)(j + 0.5) * boxSize + offsetY);
                    }
                }

                //TopLine
                canvas.drawLineOn(i*boxSize+offsetX, j*boxSize+offsetY, (i+1)*boxSize+offsetX,j*boxSize+offsetY, box.getTopLine());

                //LeftLine
                canvas.drawLineOn(i*boxSize+offsetX, j*boxSize+offsetY, (i)*boxSize+offsetX,(j+1)*boxSize+offsetY, box.getLeftLine());

                //BottomLine
                canvas.drawLineOn(i*boxSize+offsetX, (j+1)*boxSize+offsetY, (i+1)*boxSize+offsetX,(j+1)*boxSize+offsetY, box.getBottomLine());

                //RightLine
                canvas.drawLineOn((i+1)*boxSize+offsetX, j*boxSize+offsetY, (i+1)*boxSize+offsetX,(j+1)*boxSize+offsetY, box.getRightLine());

                // bottomLeftIntersect
                canvas.drawIntersection((i+1)*boxSize+offsetX, j*boxSize+offsetY, box.getBottomLeft());
               // topLeftIntersect
                canvas.drawIntersection(i*boxSize+offsetX, j*boxSize+offsetY, box.getTopLeft());
               // bottomRightIntersect
                canvas.drawIntersection((i+1)*boxSize+offsetX, (j+1)*boxSize+offsetY, box.getBottomRight());
                // topRightIntersect
                canvas.drawIntersection(i*boxSize+offsetX, (j+1)*boxSize+offsetY, box.getTopRight());
            }
        }
        //canvas.invalidate();
    }
}