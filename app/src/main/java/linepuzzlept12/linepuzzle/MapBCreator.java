package linepuzzlept12.linepuzzle;

import android.graphics.Point;

/**
 * Created by mattijn on 26/04/17.
 */

public class MapBCreator {
    private final int[] dots;
    private final int heigth;
    private final int width;
    private final int[] x;
    private final int[] y;
    private final Point start;
    private final Point end;

    public MapBCreator(int  heigth, int width,
                       int[] x, int[] y, int[] dots,
                       int startX, int startY, int endX, int endY){
        this.heigth = heigth;
        this.width = width;
        this.x = x;
        this.y = y;
        this.dots = dots;
        this.start = new Point(startX, startY);
        this.end  = new Point(endX, endY);
    }
    public Grid generateGrid(){
        return new GridA(width, heigth, start, end, new MapSettingsGenerator() {
            @Override
            public BoxTriangle setTriangles(BoxTriangle boxTriangle) {
               return boxTriangle;
            }

            @Override
            public Line setDot(Line line, int boxX, int boxY, LocationInBox loc) {
                if(line instanceof LineDot){
                    LineDot lineDot =  (LineDot) line;
                    for (int i = 0; i < x.length; i++){
                        if (x[i] == boxX && y[i] ==  boxY){
                            if (loc.value() == dots[i]){
                                lineDot.setDot();
                                return lineDot;
                            }
                        }
                    }
                }
                return line;
            }

            @Override
            public Intersection setDot(Intersection intersection, int boxX, int boxY, LocationInBox loc) {
                if(intersection instanceof IntersectionDot){
                    IntersectionDot intersectionDot =  (IntersectionDot) intersection;
                    for (int i = 0; i < x.length; i++){
                        if (x[i] == boxX && y[i] ==  boxY){
                            if (loc.value() == dots[i]){
                                intersectionDot.setDot();
                                return intersectionDot;
                            }
                        }
                    }
                }
                return intersection;
            }
        });
    }
}
