package linepuzzlept12.linepuzzle;

import android.graphics.Point;

/**
 * Created by mattijn on 26/04/17.
 */

public class GridA extends Grid{

    public GridA(int width, int height, Point start, Point end, MapSettingsGenerator msg) {
        super(width, height, start, end, new CheckerA(), msg);
    }

    @Override
    public Box createBox(int x, int y, Line topLine, Line bottomLine, Line leftLine, Line rightLine, Intersection topLeft, Intersection topRight, Intersection bottomLeft, Intersection bottomRight) {

        BoxTriangle result = new BoxTriangle(x,y,topLine, bottomLine, leftLine, rightLine, topLeft, topRight, bottomLeft, bottomRight, 0);
        super.getMapSettingsGenerator().setTriangles(result);
        return result;
    }

    @Override
    public Line createLine() {
        return new Line();
    }

    @Override
    public Intersection createIntersection() {
        return new Intersection();
    }
}
