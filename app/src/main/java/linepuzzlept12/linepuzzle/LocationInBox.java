package linepuzzlept12.linepuzzle;

/**
 * Created by mattijn on 26/04/17.
 */

public enum LocationInBox {
    topLeft(0),
    topRight(1),
    bottomRight(2),
    bottomLeft(3),
    invalid(4),
    top(5),
    right(6),
    bottom(7),
    left(8);


    private final int value;
    LocationInBox(int value){
        this.value  =  value;
    }

       public int value(){
        return value;
    }

    public LocationInBox fromValue(int value){
        switch (value){
            case 0:
                return topLeft;
            case 1:
                return topRight;
            case 2:
                return bottomRight;
            case 3:
                return bottomLeft;
            case 5:
                return top;
            case 6:
                return right;
            case 7:
                return bottom;
            case 8:
                return left;
            default:
                return invalid;
        }
    }
}
