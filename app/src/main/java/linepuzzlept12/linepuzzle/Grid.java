
package linepuzzlept12.linepuzzle;

import android.graphics.Point;
import java.util.Iterator;

/**
 * Created by Mark ten Klooster on 19-4-2017.
 */
public abstract class Grid {

    private final OnFinishedListener onFinish;
    private final MapSettingsGenerator mapSettingsGenerator;
    private int width;
    private int height;

    private Point start;
    private Point end;

    private Box[][] grid;

    public Grid(int width, int height, Point start, Point end, OnFinishedListener onFinish,
                MapSettingsGenerator mapSettingsGenerator){
        this.width = width;
        this.height = height;
        this.start = start;
        this.end = end;
        this.onFinish = onFinish;
        this.grid = new Box[width][height];
        this.mapSettingsGenerator = mapSettingsGenerator;
    }
    public abstract Box createBox(int x, int y, Line topLine, Line bottomLine, Line leftLine, Line rightLine, Intersection topLeft, Intersection topRight, Intersection bottomLeft, Intersection bottomRight);

    public abstract Line createLine();
    public abstract Intersection createIntersection();

    public void fillGrid(){
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){

                Line topLine;
                Line bottomLine;
                Line leftLine;
                Line rightLine;

                Intersection topLeft;
                Intersection topRight;
                Intersection bottomLeft;
                Intersection bottomRight;

                if(i == 0 && j == 0){
                    topLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.top);
                    bottomLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.bottom);
                    leftLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.left);
                    rightLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.right);

                    topLeft = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.topLeft);
                    topRight = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.topRight);
                    bottomLeft = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.bottomLeft);
                    bottomRight = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.bottomRight);
                }else if(j == 0){
                    topLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.top);
                    bottomLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.bottom);
                    leftLine = mapSettingsGenerator.setDot(grid[i-1][j].getRightLine(), i,j, LocationInBox.left);
                    rightLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.right);

                    topLeft = mapSettingsGenerator.setDot(grid[i-1][j].getTopRight(), i,j, LocationInBox.topLeft);
                    topRight = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.topRight);
                    bottomLeft = mapSettingsGenerator.setDot(grid[i-1][j].getBottomLeft(), i,j, LocationInBox.bottomLeft);
                    bottomRight = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.bottomRight);
                }else if(i == 0){
                    topLine = mapSettingsGenerator.setDot(grid[i][j-1].getBottomLine(), i,j, LocationInBox.top);
                    bottomLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.bottom);
                    leftLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.left);
                    rightLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.right);

                    topLeft = mapSettingsGenerator.setDot(grid[i][j-1].getBottomLeft(), i,j, LocationInBox.topLeft);
                    topRight = mapSettingsGenerator.setDot(grid[i][j-1].getBottomRight(), i,j, LocationInBox.topRight);
                    bottomLeft = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.bottomLeft);
                    bottomRight = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.bottomRight);
                }else{
                    topLine = mapSettingsGenerator.setDot(grid[i][j-1].getBottomLine(), i,j, LocationInBox.top);
                    bottomLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.bottom);
                    leftLine = mapSettingsGenerator.setDot(grid[i-1][j].getRightLine(), i,j, LocationInBox.bottom);
                    rightLine = mapSettingsGenerator.setDot(createLine(), i,j, LocationInBox.right);

                    topLeft = mapSettingsGenerator.setDot(grid[i-1][j].getTopRight(), i,j, LocationInBox.topLeft);
                    topRight = mapSettingsGenerator.setDot(grid[i][j-1].getBottomRight(), i,j, LocationInBox.topRight);
                    bottomLeft = mapSettingsGenerator.setDot(grid[i-1][j].getBottomRight(), i,j, LocationInBox.bottomLeft);
                    bottomRight = mapSettingsGenerator.setDot(createIntersection(), i,j, LocationInBox.bottomRight);
                }
                grid[i][j] = createBox(i,  j,
                        topLine, bottomLine, leftLine, rightLine,
                        topLeft, topRight, bottomLeft, bottomRight);
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Box getBox(int x, int y){
        if (x < 0 || y < 0 || x >= width || y >= height){
            return null;
        }
        return grid[x][y];
    }

    public Iterator<Box> getBoxes() {
        return new Iterator<Box>() {
            private int x = 0;
            private int y = 0;
            @Override
            public boolean hasNext() {
                return this.x < width && this.y < height;
            }

            @Override
            public Box next() {
                Box result = getBox(x,y);
                x++;
                if (x >= width){
                    x = 0;
                    y++;
                }
                return result;
            }
        };
    }
    public boolean validate(){
        return this.onFinish.check(this);
    }

    public MapSettingsGenerator getMapSettingsGenerator() {
        return mapSettingsGenerator;
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }
}
