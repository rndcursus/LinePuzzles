package linepuzzlept12.linepuzzle;

import android.graphics.Point;

/**
 * Created by mattijn on 26/04/17.
 */

public class GridB extends Grid {
    public GridB(int width, int height, Point start, Point end, MapSettingsGenerator msg) {
        super(width, height, start, end, new CheckerB(), msg);
    }

    @Override
    public Box createBox(int x, int y, Line topLine, Line bottomLine, Line leftLine, Line rightLine, Intersection topLeft, Intersection topRight, Intersection bottomLeft, Intersection bottomRight) {
        return new Box(x,y, topLine, bottomLine,leftLine,rightLine,topLeft,topRight,bottomLeft,bottomRight);
    }

    @Override
    public Line createLine(){
        LineDot result = new LineDot();
        return result;
    }

    @Override
    public Intersection createIntersection(){

        Intersection result= new Intersection();
        return result;
    }
}
