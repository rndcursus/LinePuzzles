package linepuzzlept12.linepuzzle;

/**
 * Created by Mark ten Klooster on 20-4-2017.
 */

public class Line {

    private boolean status;

    public Line(){
        this.status = false;
    }

    public boolean getStatus(){
        return status;
    }

    public void setStatus(boolean status){
        this.status = status;
    }

}

