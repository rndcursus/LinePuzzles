package linepuzzlept12.linepuzzle;

import java.util.Iterator;

/**
 * Created by mattijn on 20/04/17.
 */

public class CheckerA implements OnFinishedListener {
    @Override
    public boolean check(Grid grid) {
        Iterator<Box> boxes = grid.getBoxes();
        while(boxes.hasNext()){
            Box b = boxes.next();
            BoxTriangle box = (BoxTriangle) b;
            if (box.activatedLines() != box.getNumberOfTriangles()&& box.getNumberOfTriangles() != 0){
                return false;
            }
        }
        return true;
    }
}
