package linepuzzlept12.linepuzzle;

/**
 * Created by mattijn on 26/04/17.
 */

public interface MapSettingsGenerator {


    public BoxTriangle setTriangles(BoxTriangle boxTriangle);
    public Line setDot(Line line, int boxX, int boxY, LocationInBox loc);
    public Intersection setDot(Intersection intersection, int boxX, int boxY, LocationInBox loc);
}
